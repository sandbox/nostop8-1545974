<?php

define('INSTAGRAM_IMAGE_SMALL', 5);
define('INSTAGRAM_IMAGE_MEDIUM', 6);
define('INSTAGRAM_IMAGE_BIG', 7);

define('INSTAGRAM_UPDATE_FREQ_DEFAULT', 60 * 60);
define('INSTAGRAM_TAGS_PATTERN', 'https://api.instagram.com/v1/tags/%s/media/recent?access_token=%s');
define('INSTAGRAM_MEDIA_PATTERN', 'https://api.instagram.com/v1/media/%s?access_token=%s');
define('INSTAGRAM_MODULE_PATH', drupal_get_path('module', 'instagram_feed'));

define('INSTAGRAM_MEDIA_UPDATE_LIMIT', 50);
define('INSTAGRAM_PROCESS_MAX', 1);

/**
 * Replace instagram shortcodes
 * e.g. [instagram tags="easter, eclipse" colorbox="1" size="6" pager="1" limit="10"]
 * 
 * @param type $content
 * @return type 
 */
function _instagram_replace_shortcode($content) {
    $matches = array();
    preg_match_all('/\[instagram.*\]/', htmlspecialchars_decode($content), $matches);
    foreach($matches[0] as $instagram) {
        $element = str_replace(array('[', ']'), array('<', '/>'), $instagram);
        $xml = new SimpleXMLElement($element);
        $attrs = array();
        foreach($xml->attributes() as $attr => $val) {
            $attrs[(string) $attr] = (string) $val;
        }
        $instagram_feed = instagram_feed_view($attrs);
        
        $content = str_replace(htmlspecialchars($instagram, ENT_QUOTES), $instagram_feed, $content);
        $content = str_replace($instagram, $instagram_feed, $content);
    }   
    return $content;
}

/**
 * Updates local instagram DB with live instagram feed
 * TODO: needs some code refactoring!!!
 */
function _instagram_update_local($tags) {
    $process_max = _instagram_var('process_max') ? _instagram_var('process_max') : INSTAGRAM_PROCESS_MAX;
    $tags = explode(',', $tags);
    $count = 0;
    foreach((array) $tags as $tag) {
        $tag = trim($tag);
        $iresult = _instagram_request(INSTAGRAM_TAGS_PATTERN, $tag);
        $url = sprintf(INSTAGRAM_TAGS_PATTERN, $tag, _instagram_var('access_token'));
        $page = 0;
        $skipped = FALSE;
        while($page < $process_max) {
            $page_count = 0;
            if($iresult['meta']['code'] == 200) {
                $update_data = array();
                foreach((array) $iresult['data'] as $key => $item) {
                    $result = _instagram_insert_item($item, $tag);
                    $title = ($key + 1) . '. ' . $item['id'];
                    if($result == 'inserted') {
                        $count++;
                        $page_count++;
                        $update_data[] = t('!title inserted in DB', array('!title' => $title));
                    } elseif($result == 'skipped') {
                        $update_data[] = t('!title already exists in DB.', array('!title' => $title));
                        $skipped = TRUE;
                        break;
                    } else {
                        $update_data[] = t('!title insert went wrong', array('!title' => $title));
                    }
                }  
                watchdog('instagram', t('Instagram inserted !count items marked with !tag in DB (page !page - !url): !items', array('!tag' => $tag, '!url' => $url, '!count' => $page_count, '!page' => $page, '!items' => "\n<pre>" . implode("\n", $update_data) . '</pre>')));
            } else {
                watchdog('instagram', t('Instagram returned unexpected error (page !page - !url): !error', array('!page' => $page, '!url' => $url, '!error' => '<pre>' . print_r($iresult['meta'], TRUE) . '</pre>')));
                break;
            }       
            if($skipped) {
                break;
            }
            if($url = $iresult['pagination']['next_url']) {
                $iresult = _instagram_request($url);
                $page++;                
            } else {
                break;
            }
        }
    }    
    watchdog('instagram', t('Instagram feed (!count items) downloaded with next tags: !tags'), array('!count' => $count, '!tags' => implode(',', $tags)), WATCHDOG_INFO);
}

/**
 * Inserts instagram item into DB
 * 
 * @param type $item 
 */
function _instagram_insert_item($item, $tag) {
    _instagram_insert_user($item['user']);
    if(db_result(db_query('SELECT COUNT(*) FROM {instagram_feed} i WHERE i.iid = "%s"', array($item['id']))))
            return 'skipped';
    
    $instagram['iid'] = $item['id'];
    $instagram['title'] = $item['caption']['text'];
    $instagram['instagram_url'] = $item['images']['standard_resolution']['url'];
    $instagram['internal_images'] = _instagram_save_images_internally($instagram['instagram_url']);
    $instagram['tag'] = $tag;
    $instagram['iuid'] = $item['user']['id'];
    $instagram['likes'] = $item['likes']['count'];
    $instagram['pubdate'] = $item['created_time'];
    $instagram['data'] = $item;
    $instagram['downloaded'] = time();
    $instagram['status'] = 1;
    $instagram['removed'] = 0;
    
    $instagram = (object) $instagram;
    if(drupal_write_record('instagram_feed', $instagram)) {
        return 'inserted';
    }
}

/**
 * Insert new instagram user
 *
 * @param type $user
 * @return type 
 */

function _instagram_insert_user($user) {
    if(db_result(db_query('SELECT COUNT(*) FROM {instagram_user} iu WHERE iu.iuid = %d', array($user['id']))))
            return;
    
    $iuser['iuid'] = $user['id'];
    $iuser['iusername'] = $user['username'];
    $iuser['ifullname'] = $user['full_name'];
    $iuser['iudata'] = $user;    
    $iuser = (object) $iuser;
    
    drupal_write_record('instagram_user', $iuser);    
}

/**
 * Updates local instagrams
 */
function _instagram_update_local_media($limit) {
    $lui = (int) variable_get('instagram_media_lui', 0); //last updated item
    $limit = $limit ? $limit : INSTAGRAM_MEDIA_UPDATE_LIMIT; //media update limit
    $query = 'SELECT 
                    i.id, 
                    i.iid,
                    i.title,
                    i.likes
                  FROM {instagram_feed} i
                    WHERE i.id > %d
                    ORDER BY i.id ASC
                    LIMIT 0, %d';
    $result = db_query($query, array($lui, $limit));
    
    $update_data = array();
    $count = 1;
    while($row = db_fetch_array($result)) {
        $iresult = _instagram_request(INSTAGRAM_MEDIA_PATTERN, $row['iid']);
        $data = $iresult['data'];
        $title = $count . '. ' . $row['title'] . ' (' . $row['id'] . ')';
        if($iresult['meta']['code'] == 400) { //searching for bad request responce
            _instagram_items_remove(array($row['id']), 'id');
            $update_data[] = t('!title: removed', array('!title' => $title));
        } elseif($iresult['meta']['code'] == 200) {
            if(!empty($data)) {
                _instagram_items_update(array($row['id']), 'likes', $data['likes']['count']);
                $update_data[] = t('!title: likes update: !from > !to', array('!title' => $title, '!from' => $row['likes'], '!to' => $data['likes']['count']));
            } else {
                $update_data[] = t('!title: empty data, no updates.', array('!title' => $title));
            }
        } else {
            $update_data[] = t('!title: returned unexpected error: !error.', array('!title' => $title, '!error' => '<pre>' . print_r($iresult['meta'], TRUE) . '</pre>'));
        }
        $lui = $row['id'];
        $count++;
    }

    if(db_result(db_query('SELECT COUNT(*) FROM {instagram_feed} i WHERE i.id > %d', array($lui)))) {
        variable_set('instagram_media_lui', $lui);
    } else { //if we get no results with higher id, then we reset our update counter
        variable_set('instagram_media_lui', 0);
    }
    watchdog('instagram', t('Local instagram media (!count) updated: !update'), array('!count' => count($update_data), '!update' => "\n<pre>" . implode("\n", $update_data) . '</pre>'), WATCHDOG_INFO);
}

/**
 * Updates instagram items
 * 
 * @param array $items instagram ids
 * @param string $field db column name: status or removed
 * @param int $value 0 or 1
 * @return type 
 */
function _instagram_items_update($items, $field, $value) {
    if(!in_array($field, array('status', 'likes')) || !is_int($value) || empty($items)) 
         return;
    return db_query('UPDATE {instagram_feed} SET %s = %d WHERE id IN (%s)', array($field, (int) $value, implode(',', $items)));
}

/**
 * Updates instagram items
 * 
 * @param array $items instagram ids
 * @param string $field db column name: status or removed
 * @param int $value 0 or 1
 * @return type 
 */
function _instagram_items_remove($items, $field) {
    if(!in_array($field, array('iid', 'id')) || empty($items)) 
         return;
    
    return db_query('DELETE FROM {instagram_feed} WHERE %s IN (%s)', array($field, implode(',', $items)));
}

/**
 * Makes external request to instagram server
 * 
 * @param type $tag
 * @return type 
 */
function _instagram_request($pattern, $arg1 = NULL) {
    if($arg1) {
        $url = sprintf($pattern, $arg1, _instagram_var('access_token'));
    } else {
        $url = $pattern;
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
    ob_start();
    curl_exec($ch); 
    $json = ob_get_contents();
    ob_end_clean();
    
    return json_decode($json, TRUE);
}

/**
 * Reads all instagram images and saves physycally
 * 
 * @param string $instagram_url Path to instagram image
 * @return string 
 */
function _instagram_save_images_internally($instagram_url) {
    $p1 = substr($instagram_url, 0, -5);
    $p2 = substr($instagram_url, -4);
    $dir = file_directory_path() . '/instagram';
    $images = array();
    if(file_check_directory($dir, FILE_CREATE_DIRECTORY)) {
        foreach(_instagram_image_sizes() as $size => $label) {
            $url = $p1 . $size . $p2;
            if($data = file_get_contents($url)) {
                $dest = $dir . '/' . preg_replace('/^http:\/\/.*\//', '', $url);
                if(file_save_data($data, $dest, FILE_EXISTS_REPLACE)) {
                    _instagram_file_save_db($dest);
                } else {
                    watchdog('instagram', 'Failed to save file internally: @path', array('@path' => $dest), WATCHDOG_ERROR);
                }
                $images[$size] = $dest; //TODO: check whether file_save_data() returns full path         
            } else {
                watchdog('instagram', 'Failed to read image data from instagram: @url', array('@url' => $url), WATCHDOG_ERROR);
            }
        }
    } else {
        watchdog('instagram', 'Failed to create instagram directory: @dir', array('@dir' => $dir), WATCHDOG_ERROR);
        return;
    }
    
    return $images;
}

/**
 * Saving info about file into DB
 * 
 * @param type $dest
 * @return type 
 */
function _instagram_file_save_db($dest) {
    $file = new stdClass();
    $file->uid = $user->uid;
    $file->filename = basename($dest);
    $file->filepath = $dest;
    // maybe better use file_get_mimetype() here ;)
    $file->filemime = getimagesize($dest);
    if ($file->filemime === false) {
        watchdog('instagram', "file type error @path", array('@path'=>$dest), WATCHDOG_ERROR);
        return false;
    }
    $file->filemime = $file->filemime['mime'];
    $file->filesize = filesize($dest);
    $file->status = FILE_STATUS_PERMANENT;
    $file->timestamp = time();

    return drupal_write_record('files', $file);    
}

/**
 * Loads instagram feeds from local DB
 * 
 * @param type $attrs
 * @return type 
 */
function _instagram_load_items($attrs = array()) {
    extract($attrs);
    
    $query = "SELECT %s 
                    FROM {instagram_feed} i
                        INNER JOIN {instagram_user} iu ON iu.iuid = i.iuid
                    WHERE i.status = 1";
    
    if(!empty($tags)) {
        $tags = implode("','", $tags);  
        $query .= " AND i.tag IN ('$tags')";
    }
    
    $query .= " ORDER BY i.$order_by $order";
    if($pager) {
        $result = pager_query(sprintf($query, '*'), $limit, $offset, sprintf($query, 'COUNT(*)'));
    } else {
        if($limit) {
           $query .= " LIMIT $offset, $limit";
        }
        $result = db_query(sprintf($query, '*'));
    }
    
    $instagrams = array();
    while($row = db_fetch_object($result)) {
        $instagrams[$row->id] = (array) $row;
        $instagrams[$row->id]['internal_images'] = unserialize($row->internal_images);
    }
    return $instagrams;
}



/**
 * Generates instagram image url
 *
 * @param type $instagram_id
 * @param type $images
 * @param type $imagecache 
 */
function _instagram_get_image_url($instagram_url, $images = null, $size = NULL) {
    $size = !$size ? INSTAGRAM_IMAGE_SMALL : $size;
    
    if(_instagram_var('use_internal_image') && !empty ($images)) {
        $url = $images[$size];
    } else {
        $ext = substr($instagram_url, -4);
        $url = substr($instagram_url, 0, -5) . $size . $ext;
    }    
    
    return $url;
}

function _instagram_image_sizes() {
        return array(
            INSTAGRAM_IMAGE_SMALL => t('Small'),
            INSTAGRAM_IMAGE_MEDIUM => t('Meduim'),
            INSTAGRAM_IMAGE_BIG => t('Big'),
        );
}

function _instagram_var($name) {
    $settings = variable_get('instagram', NULL);
    return $settings[$name];
}