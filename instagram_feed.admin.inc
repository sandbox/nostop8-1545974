<?php

function instagram_feed_settings($form_state) {
    $form = array();
    $form['#tree'] = TRUE;
    
    $settings = variable_get('instagram', null);

    $form['instagram']['access_token'] = array(
        '#type' => 'textfield',
        '#title' => t('Access Token'),
        '#description' => t('Please, enter your access token to fetch feeds form instagram.'),
        '#default_value' => $settings['access_token']
    );     
    
    $form['instagram']['tags'] = array(
        '#type' => 'textfield',
        '#title' => t('Instagram tags'),
        '#description' => t('Enter instagram tags separated by comma, e.g.: eclipse, sweets, coffee'),
        '#default_value' => $settings['tags']
    );
    
    $form['instagram']['use_internal_image'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use internal images'),
        '#description' => t('Whether to use internal copies of images (in-case instagram service is unavailable)'),
        '#default_value' => $settings['use_internal_image']
    ); 
    
    for($i = 1; $i <= 10; $i++) {
        $max[$i] = format_plural($i , '1 page', '@count pages');
    }    
    $form['instagram']['process_max'] = array(
        '#type' => 'select',
        '#title' => t('Max instagram pages to process'),
        '#description' => t('The maximum number of pages that could be downloaded from the instagram and processed while already downloaded media won\'t be found in DB.'),
        '#options' => $max,
        '#default_value' => $settings['process_max'] ? $settings['process_max'] : INSTAGRAM_PROCESS_MAX
    );    
    
    $form['instagram']['live_download'] = array(
        '#type' => 'checkbox',
        '#title' => t('Automatic feed download'),
        '#description' => t('Turn on automatic download of instagram feed on cron run.'),
        '#default_value' => $settings['live_download']
    ); 
    $form['instagram']['live_update'] = array(
        '#type' => 'checkbox',
        '#title' => t('Automatic feed update'),
        '#description' => t('Turn on automatic update of local instagram media on cron run.'),
        '#default_value' => $settings['live_update']
    );     
    
    for($i = 1; $i <= 60; $i = $i + 5) {
        $t = $i;
        if($i > 1) {
            $t--;
        }
        $freq[$t * 60] = format_plural($t, '1 minute', '@count minutes');
    }    
    for($i = 1; $i <= 24; $i++) {
        $freq[$i * 3600] = format_plural($i, '1 hour', '@count hours');
    }
    $form['instagram']['update_freq'] = array(
        '#type' => 'select',
        '#title' => t('Update Frequency'),
        '#options' => $freq,
        '#default_value' => $settings['update_freq'] ? $settings['update_freq'] : INSTAGRAM_UPDATE_FREQ_DEFAULT
    );
    
    for($i = 50; $i <= 500; $i = $i + 50) {
        $limit[$i] = format_plural($i , '1 item', '@count items');
    }    
    $form['instagram']['media_update_limit'] = array(
        '#type' => 'select',
        '#title' => t('Media Update Limit'),
        '#description' => t('Count of media items to update per cron run. In case media is removed at instagram, it will be removed form local DB too.'),
        '#options' => $limit,
        '#default_value' => $settings['media_update_limit'] ? $settings['media_update_limit'] : INSTAGRAM_MEDIA_UPDATE_LIMIT
    );
    
    return system_settings_form($form);
}

function instagram_feed_manage($form_state) {
  $header = array(
    array('data' => t('Instagram'), 'field' => 'i.id', 'class' => 'id', 'sort' => 'desc'),
    array('data' => t('Title'), 'field' => 'i.title', 'class' => 'title'),
    array('data' => t('User'), 'field' => 'iu.iusername', 'class' => 'user'),
    array('data' => t('Likes'), 'field' => 'i.likes', 'class' => 'likes'),
    array('data' => t('Tag'), 'field' => 'i.tag', 'class' => 'tag'),
    array('data' => t('Status'), 'field' => 'i.status', 'class' => 'status'),
    array('data' => t('Created'), 'field' => 'i.pubdate', 'class' => 'created'),
    array('data' => t('Downloaded'), 'field' => 'i.downloaded', 'class' => 'downloaded'),
    //table_select_header_cell to be inserted in the theme function
  );    
  $form['header'] = array(
      '#type' => 'value',
      '#value' => $header
  );
    
  $result = pager_query('SELECT 
                                    i.id,
                                    i.title,
                                    i.instagram_url,
                                    i.internal_images,
                                    i.tag,
                                    i.likes,
                                    i.pubdate,
                                    i.downloaded,
                                    i.status,
                                    iu.iusername,
                                    iu.ifullname,
                                    iu.iudata
                            FROM {instagram_feed} i
                                INNER JOIN {instagram_user} iu ON iu.iuid = i.iuid
                            ' . tablesort_sql($header), 
                         20, 
                         0, 
                         'SELECT 
                             COUNT(*) 
                             FROM {instagram_feed} i');
  
  $instagrams = array();
  while($row = db_fetch_array($result)) {
      $id = $row['id'];
      $row['internal_images'] = unserialize($row['internal_images']);
      
      $instagrams[$id] = '';
      $form['instagram'][$id] = array(
          '#value' => theme('instagram_image', $row, array('colorbox' => TRUE, 'size' => INSTAGRAM_IMAGE_SMALL))
      );
      $form['title'][$id] = array(
          '#value' => $row['title']
      );      
      $form['user'][$id] = array(
          '#value' => $row['iusername'] . ' (' . $row['ifullname'] . ')'
      );
      $form['likes'][$id] = array(
          '#value' => $row['likes']
      );
      $form['tag'][$id] = array(
          '#value' => $row['tag']
      );       
      $form['status'][$id] = array(
          '#value' => $row['status'] ? '<span class="status published" title="Published"></span>' : '<span class="status unpublished" title="Unpublished"></span>'
      );       
      $form['pubdate'][$id] = array(
          '#value' => format_date($row['pubdate'], 'custom', 'Y-m-d H:i')
      );  
      $form['downloaded'][$id] = array(
          '#value' => format_date($row['downloaded'], 'custom', 'Y-m-d H:i')
      );       
  }
  $form['instagrams'] = array(
      '#type' => 'checkboxes',
      '#options' => $instagrams
  );
  
  $form['#theme'] = 'instagram_feed_manage';
  $form['pager'] = array('#value' => theme('pager', NULL, 20, 0));
  
  $form['action'] = array(
      '#type' => 'select',
      '#title' => t('Action'),
      '#options' => array(
          'publish' => t('Publish'),
          'unpublish' => t('Unpublish'),
          'remove' => t('Remove')
      ),
  );  
  
  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
      '#name' => 'submit'
  );
  
  $form['download'] = array(
      '#type' => 'submit',
      '#value' => t('Download feed manually'),
      '#name' => 'download'
  );
  
  $form['update'] = array(
      '#type' => 'submit',
      '#value' => t('Update media info'),
      '#name' => 'update'
  );  
  
  return $form;
}

function instagram_feed_manage_submit($form, &$form_state) {
    $values = $form_state['values'];
    $settings = variable_get('instagram', null);
    switch($form_state['clicked_button']['#name']) {
        case 'download': 
            _instagram_update_local($settings['tags']);
            break;
        case 'update':
            _instagram_update_local_media(_instagram_var('media_update_limit'));
            break;
        case 'submit':
            $instagrams = array();
            foreach((array) $values['instagrams'] as $key => $val) {
                if($val) {
                    $instagrams[] = $key;
                }
            }
            if(!empty ($instagrams)) {
                switch($values['action']) {
                    case 'publish':
                        _instagram_items_update($instagrams, 'status', 1);
                        break;
                    case 'unpublish':
                        _instagram_items_update($instagrams, 'status', 0);
                        break;
                    case 'remove':
                        _instagram_items_remove($instagrams, 'id');
                        break;
                }                
            }
            break;
    }
}

function theme_instagram_feed_manage($form) {
  $has_posts = isset($form['instagram']) && is_array($form['instagram']);
  
  $select_header = $has_posts ? theme('table_select_header_cell') : '';    
  $header = $form['header']['#value'];
  $header[] = $select_header;
  
  $output = '';
  if ($has_posts) {
    foreach (element_children($form['instagram']) as $key) {
      $row = array();
      $row[] = array('data' => drupal_render($form['instagram'][$key])); 
      $row[] = array('data' => drupal_render($form['title'][$key]), 'class' => 'title'); 
      $row[] = array('data' => drupal_render($form['user'][$key]), 'class' => 'user'); 
      $row[] = array('data' => drupal_render($form['likes'][$key]), 'class' => 'likes'); 
      $row[] = drupal_render($form['tag'][$key]);
      $row[] = array('data' => drupal_render($form['status'][$key]), 'class' => 'status');
      $row[] = drupal_render($form['pubdate'][$key]);
      $row[] = drupal_render($form['downloaded'][$key]);
      $row[] = drupal_render($form['instagrams'][$key]);
      $rows[] = $row;
    }

  }
  else {
    $rows[] = array(array('data' => t('No posts available.'), 'colspan' => '8'));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;  
}