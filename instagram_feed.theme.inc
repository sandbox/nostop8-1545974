<?php

function theme_instagram_item($item, $attrs = array()) {
    static $colorbox_added = FALSE;
    if($attrs['colorbox'] && !$colorbox_added) {
        drupal_add_css(INSTAGRAM_MODULE_PATH . '/js/colorbox/colorbox.css');
        drupal_add_js(INSTAGRAM_MODULE_PATH . '/js/colorbox/jquery.colorbox-min.js');
        drupal_add_js(INSTAGRAM_MODULE_PATH . '/js/instagram_feed.js');
    }
    
    $output = array();
    
    if($attrs['likes']) {
        $likes = '<span class="likes">' . format_plural($item['likes'], '1 like', '@count likes') . '</span>';
    }
    if($attrs['user']) {
        $user = '<span class="iuser">' . $item['iusername'] . '</span>';
    }
    
    $output[] = '<div class="instagram-item">';
    $output[] = '<div class="iimage">' . theme('instagram_image', $item, $attrs) . '</div>';
    $output[] = '<div class="iinfo clearfix">' . $user . $likes . '</div>';
    $output[] = '</div>';
    
    return implode("\n", $output);
}

function theme_instagram_image($item, $attrs = array()) {
    static $colorbox_added = FALSE;
    if($attrs['colorbox'] && !$colorbox_added) {
        drupal_add_css(INSTAGRAM_MODULE_PATH . '/js/colorbox/colorbox.css');
        drupal_add_js(INSTAGRAM_MODULE_PATH . '/js/colorbox/jquery.colorbox-min.js');
        drupal_add_js(INSTAGRAM_MODULE_PATH . '/js/instagram_feed.js');
    }
    
    $url = _instagram_get_image_url($item['instagram_url'], $item['internal_images'], $attrs['size']);
    if(_instagram_var('use_internal_image') && !empty ($item['internal_images'])) {
        $image = theme('image', $url, $item['title'], $item['iusername'], array('class' => 'instagram-image'));
    } else {
        $image = theme('image', $url, $item['title'], $item['iusername'], array('class' => 'instagram-image'), FALSE);
    }
    
    if($attrs['colorbox']) {
        $image = l($image, _instagram_get_image_url($item['instagram_url'], $item['internal_images'], INSTAGRAM_IMAGE_BIG), array('html' => TRUE, 'attributes' => array('class' => 'instagram-colorbox', 'rel' => 'colorbox', 'title' => $item['title'])));
    }
    
    return $image;
}

function theme_instagram_feed_view($items, $attrs) {
    if(empty($items))
        return;
    extract($attrs);
    $output = array();
    $output[] = '<div class="instagram-feed ' . implode(' ', $attrs['tags']) . ' clearfix">';
    foreach($items as $item) {
        $output[] = theme('instagram_item', $item, $attrs);
    }
    $output[] = '</div>'; 
    if($pager) {
        $output[] = theme('pager', $attrs['limit'], $attrs['offset']);
    }
    
    return implode("\n", $output);
}